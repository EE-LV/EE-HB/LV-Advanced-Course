﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="12008004">
	<Property Name="CCSymbols" Type="Str">debugLevel,1;</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="varPersistentID:{3AB1CF12-5C0C-43E5-A974-46EA442AD0FE}" Type="Ref">/My Computer/exercises/mySharedVariables.lvlib/SVCalculatorCommand</Property>
	<Property Name="varPersistentID:{6C13A869-8883-43CE-B024-B846B27C7FAE}" Type="Ref">/My Computer/exercises/mySharedVariables.lvlib/simple/testVariable</Property>
	<Property Name="varPersistentID:{D3C19E8D-AD20-445B-B351-B09533AB62D9}" Type="Ref">/My Computer/exercises/mySharedVariables.lvlib/SVCalculatorResult</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="exercises" Type="Folder">
			<Item Name="importantExercises.lvlib" Type="Library" URL="../exercises/importantExercises/importantExercises.lvlib"/>
			<Item Name="simpleAdd.vi" Type="VI" URL="../simpleAdd.vi"/>
			<Item Name="VIServerCalculator.vi" Type="VI" URL="../VIServerCalculator.vi"/>
			<Item Name="VIServerCalculator2.vi" Type="VI" URL="../VIServerCalculator2.vi"/>
			<Item Name="mySharedVariables.lvlib" Type="Library" URL="../exercises/mySharedVariables.lvlib"/>
		</Item>
		<Item Name="examples" Type="Folder">
			<Item Name="importantFeatures.lvlib" Type="Library" URL="../examples/importantFeatures/importantFeatures.lvlib"/>
			<Item Name="test1.vi" Type="VI" URL="../test1.vi"/>
			<Item Name="test2.vit" Type="VI" URL="../test2.vit"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="NI_PtbyPt.lvlib" Type="Library" URL="/&lt;vilib&gt;/ptbypt/NI_PtbyPt.lvlib"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="Create Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Create Rendezvous.vi"/>
				<Item Name="Rendezvous RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Rendezvous RefNum"/>
				<Item Name="Wait at Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Wait at Rendezvous.vi"/>
				<Item Name="Destroy Rendezvous.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/rendezvs.llb/Destroy Rendezvous.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
			</Item>
			<Item Name="Semaphore User 3.vi" Type="VI" URL="../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/semaphore.llb/Semaphore User 3.vi"/>
			<Item Name="Semaphore Example - Config R.vi" Type="VI" URL="../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/semaphore.llb/Semaphore Example - Config R.vi"/>
			<Item Name="Semaphore Example - Data Dev.vi" Type="VI" URL="../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/semaphore.llb/Semaphore Example - Data Dev.vi"/>
			<Item Name="Semaphore Example - Function.vi" Type="VI" URL="../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/semaphore.llb/Semaphore Example - Function.vi"/>
			<Item Name="Semaphore Example - Read Dat.vi" Type="VI" URL="../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/semaphore.llb/Semaphore Example - Read Dat.vi"/>
			<Item Name="Semaphore User 2.vi" Type="VI" URL="../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/semaphore.llb/Semaphore User 2.vi"/>
			<Item Name="Semaphore User 1.vi" Type="VI" URL="../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/semaphore.llb/Semaphore User 1.vi"/>
			<Item Name="Rendezvous User 3.vi" Type="VI" URL="../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/rendezvous.llb/Rendezvous User 3.vi"/>
			<Item Name="Rendezvous User 2.vi" Type="VI" URL="../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/rendezvous.llb/Rendezvous User 2.vi"/>
			<Item Name="Rendezvous User 1.vi" Type="VI" URL="../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/rendezvous.llb/Rendezvous User 1.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
