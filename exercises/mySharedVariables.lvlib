﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="12008004">
	<Property Name="NI.Lib.Icon" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!,X!!!*Q(C=T:3R&lt;B."%)&lt;`1S#:UG]1YD,OJK*%#F*?)"XVC*)O&amp;6*I5&amp;ZB/DIM3YA[-I)KX432YD)P1"&amp;2)3OS/,\&lt;L*%6/_='*':PVP&lt;`\]Z_N\=_K=2!?KRW6ZTX2I^@ZD\MNS8\[_`Q?_P8&amp;8LYTHP^/[0(,ZOT?X^WB8I$PX&gt;/Z^_OR?*W@$O_\[`.X/Z@8FTC&gt;@VC?&lt;$=^#^I&lt;?E89_+_@YFTW::_=&lt;!]W0"8*PVC/&gt;CMPSJ/P^6P+VT(P[X__PVNY6OMRR:`P/T%WG`RF`0/7I[7$`DDKUY=F7O&lt;X]Y"'Z6LW`-DLO&lt;3;+3Z.G)V^GJ/W\3V_`TM0H[HOQ`JPQWJ+;2EEE%[;?8OWE20^%20^%20^%!0^%!0^%!0&gt;%&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%-X&gt;%-X&gt;%-X^.,1B3ZUI&lt;-LS?,*1EH2J%!S'"1F8=+4]#1]#1]`F@!E0!F0QJ0Q-%1*4]+4]#1]#1`4F0!E0!F0QJ0Q5+K1:'HI]#1]F&amp;@!%`!%0!&amp;0Q-/3#HA#A'#RI("1"!Q&amp;:P!FY!FY!B[_+O!*?!+?A#@AQ6&lt;!%`!%0!&amp;0Q-/5MCN2;,K'$A^FZ0!Y0![0Q_0Q5&amp;I/D]0D]$A]$A`,S?&amp;R?"Q):U'H/!BS*DE$H"]/D]0$BRQ?B]@B=8A=(KTSB,TM4%@4.82Y$"[$R_!R?!Q?3MDA-8A-(I0(Y+'M$"[$R_!R?!Q?FJ,"9`!90!;)M3D,SSBG4$1''9,"QV6/CZ7H&amp;)8%3KN`T&lt;O$KHI!61_7[I&amp;201CK$VDVQ;E_%.7.6NV!V9V2P7(6'V%&amp;6&amp;V9N;$K1.X18Z-T=EK?E3@E-8F)\J0$&lt;OJ@(HBT=[0L[WP.:D..JV/&gt;H:XJZ/2%R]@(/DQ]V0\_PI&lt;$Y:_XV2\N,JL6?_G5\_2T]C8ZB(R+$MFHZ!PS&lt;4&gt;G.@:&lt;OWA_;.*]`D6J0PW=..0P!`+I_@BK5P,TVS/]!7-'T72PU(RZQ_@2I-R\`VL.OR^FP?&lt;OP@1`P"PV3/X;;ZY^_AVK-_@D!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">302022660</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="simple" Type="Folder">
		<Item Name="testVariable" Type="Variable">
			<Property Name="featurePacks" Type="Str">Network</Property>
			<Property Name="Network:BuffSize" Type="Str">50</Property>
			<Property Name="Network:SingleWriter" Type="Str">False</Property>
			<Property Name="Network:UseBinding" Type="Str">False</Property>
			<Property Name="Network:UseBuffering" Type="Str">False</Property>
			<Property Name="numTypedefs" Type="UInt">0</Property>
			<Property Name="type" Type="Str">Network</Property>
			<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!B(1!!!")!A!1!!!!"!!5!#A!!!1!!!!!!!!!!!!!!!!!!!!!!!!</Property>
		</Item>
		<Item Name="testServer.vi" Type="VI" URL="../testServer.vi"/>
		<Item Name="testClient.vi" Type="VI" URL="../testClient.vi"/>
	</Item>
	<Item Name="SVCalculatorCommand" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"?7A!!!")!A!1!!!!%!""!-0````](9W^N&lt;7&amp;O:!!(1!I!!6A!"U!+!!&amp;:!"B!5!!$!!!!!1!##W.P&lt;7VB&lt;G25?8"F!!%!!Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="SVCalculatorResult" Type="Variable">
		<Property Name="featurePacks" Type="Str">Network</Property>
		<Property Name="Network:BuffSize" Type="Str">50</Property>
		<Property Name="Network:SingleWriter" Type="Str">False</Property>
		<Property Name="Network:UseBinding" Type="Str">False</Property>
		<Property Name="Network:UseBuffering" Type="Str">False</Property>
		<Property Name="numTypedefs" Type="UInt">0</Property>
		<Property Name="type" Type="Str">Network</Property>
		<Property Name="typeDesc" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!#$@Q!!!")!A!1!!!!'!!&gt;!#A!"7A!11$$`````"W.P&lt;7VB&lt;G1!"U!+!!&amp;9!!&gt;!#A!"71!91&amp;!!!Q!"!!)!!QND&lt;WVN97ZE6(FQ:1!71&amp;!!!A!!!!1+=G6T&gt;7RU6(FQ:1!!!1!&amp;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!</Property>
	</Item>
	<Item Name="SV distributedCalculator processor.vi" Type="VI" URL="../importantExercises/SV distributedCalculator processor.vi"/>
</Library>
