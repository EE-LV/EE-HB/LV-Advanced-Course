﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="12008004">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">Contains some examples to explain LabVIEW features. Some examples are taken directly from the LabVIEW examples. Other examples are influenced by work done within the "LabVIEW User Group Central Europe".

Hier sind jetzt auch noch die Uebungen

Lizenz GPL, ....
Kontakt: 

author: ich
maintainer: Du



history:
28-Jan-2008: Dietrich Beck, GSI-Darmstadt</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!-]!!!*Q(C=T:3R&lt;B."%)&lt;(C-*N#HLE\9C\A4=Q5FK+P-+)ECZN;&amp;!;'KC'#DL)'U2"IH+".)WFO-QL2*27:,&amp;]O\=W:V^1'J$9^?T&gt;`@`O\(?\[R/J:3TS50*O3:2&gt;Z;*@5INNO&gt;P0@`$4NOH\^6?&lt;6"Z4^^#-P0'+83ZF?*6KG\P&lt;A&lt;`NU]`"KVX5/8,(F@-/8RK]X=8!4`@Z&amp;`P_9((XVF&gt;[*@&amp;,K;`A\S9I,Z&amp;X`&gt;P@J=RQ/^XX?QF7N^-\`-6]A&lt;G9J\R;([[(`JS;]TT.]WJ+W@=8_)O]3,3LQ`8BQ#]G82,N;JW'_7PSYO?\`6TB&amp;G8B6CE.]`?7.[8#RRKW:3T_KF@3&gt;/B0VU7M&lt;&lt;8X`@7S7!6M[W`M&lt;PT6N"O;JH8[CN(@PW7OOZ[W2W#(HX+V,"MP3RG54&gt;_L*86IS_\ZO=O`J^$H^.Z/`\C)D#IJ%9142GB^ORTIA2\IA2\IA?\IDO\IDO\IDG\IBG\IBG\IBK\ICK\ICK\ICFYLOK!,OK#T+M(EQ52"UC""U"E5#:K!*_!*?!)?(C8A#8A#HI!HY+',"$Q"4]!4]!1]$*/!*_!*?!+?A)&gt;5F32K29=HY#'^/$Q/D]0D]$A]4#E/DQ0A4/9E&gt;J+!)9\JX$A]$I`$Q[UY0![0Q_0Q/$T9YP!Y0![0Q_0Q-+3OCF?;5N(B)9U90!;0Q70Q'$SE&amp;I0(Y$&amp;Y$"[$B_H%Y$&amp;Y$!BD1C-Z#')--DI9$Q;0Q=.&amp;$"[$R_!R?!Q?L,J$6F?GU*3+$I`#I`!I0!K0QE-+58A5(I6(Y6&amp;Y3#M+D]+D]#A]#A^4C=+D]#A]#IAS+&gt;/,EEQ:K(23")7(8TUN7H@*+YH7WP[;X5'6&gt;A#F(3RJ"U&lt;;1:#WQ&gt;)W4NK'3&amp;NI;1MI&lt;7'EP&lt;#U&amp;Z%'+'VC;1GF&gt;:1&lt;WGPCEDAHTIA4YJC9%20CI!T^SRVP&lt;G\E_PJ;,C]PZ@T]8-\/TO4EZ%3/DY^F.JP*:$+2AY/$\&gt;@K-&lt;5LI]VX[:4\5XEY+N&gt;X\Z^_``,E_;.0E^G,$R_?P?W],D:^P_86[.P0T\U9%U?^Z[/K@:4R[00D]?DL+[Z(YTLOT5M:P@\R/^``]7W5"Z*\HXH7["&gt;]KMOI!!!!!!</Property>
	<Property Name="NI.Lib.LocalName" Type="Str">importantExercises</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">302022660</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="Serialized ACL" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="eventStructureGUI" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="calculatorGUI.vi" Type="VI" URL="../calculatorGUI.vi"/>
	</Item>
	<Item Name="distributedCalculator1" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="distributedCalculator1 processor.vi" Type="VI" URL="../distributedCalculator1 processor.vi"/>
		<Item Name="distributedCalculator1 command type.ctl" Type="VI" URL="../distributedCalculator1 command type.ctl"/>
		<Item Name="distributedCalculator1 result type.ctl" Type="VI" URL="../distributedCalculator1 result type.ctl"/>
		<Item Name="distributedCalculator1 GUI.vi" Type="VI" URL="../distributedCalculator1 GUI.vi"/>
		<Item Name="distributedCalculator1 another GUI.vi" Type="VI" URL="../distributedCalculator1 another GUI.vi"/>
	</Item>
</Library>
