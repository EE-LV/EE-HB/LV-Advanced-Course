﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="12008004">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">Contains some examples to explain LabVIEW features. Some examples are taken directly from the LabVIEW examples. Other examples are influenced by work done within the "LabVIEW User Group Central Europe".

history:
25-Jan-2008: Dietrich Beck, GSI-Darmstadt</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!-,!!!*Q(C=T:3R&lt;B."%)&lt;`1S#:UG]1YD,OJK*%#F*?)"XVC*)O&amp;6*I5&amp;ZB/DIM3SA0%!2JUEU4+3\T!B12&amp;&lt;)CC_/\^4IRM2-8A-3O:XX_`^W:\`&lt;7*Z87ER[LX&gt;2/(WRT``Z*R6_S\][=_WURWHG5XN;JC`8N[@SC8NYK.`ZPRG,Z&lt;@WW8;\4,J@?@(^`YG^K?L$B0\CG][_8WP2[?$W][S_N8/_@HZXD&gt;?.UND.&lt;^=`I&lt;2GH1^J&gt;`RTHP#XD&gt;'?WM_)P4-&lt;JL,?;@Z'==;X@6LC/@VX_Z@N&lt;QT&gt;&gt;&lt;GP]Y;Q4[\D'HUU[;T;9X?-0,TJR5$\L`(9#W+"]VDU`WM6%'AQUU5J&lt;T,W9U&amp;&gt;N&lt;4Y`GY`@Y?:$_G_&lt;V"23)IEAH,"S&gt;WWC*XKC*XKC*XKA"XKA"XKA"\KD/\KD/\KD/\KB'\KB'\KB'XLJ[%)8ON$:F;2Y5CB*GC2)*I/C:%BY%J[%*_(BJR+?B#@B38A3(K9IY5FY%J[%*_&amp;BG2+?B#@B38A3(F)6ECQ&gt;(:[%B`1+?!+?A#@A#8AIK9!H!!C+"9G$*'!I-)/,A#@A#8CY6-!4]!1]!5`!A[W!*_!*?!+?A)=F:6?CU(1&gt;(2\3S/&amp;R?"Q?B]@B)&lt;5=(I@(Y8&amp;Y("\+S?&amp;R?"Q)J[#4(!1ZCZQ*TA_(R_(B3Q[0Q_0Q/$Q/$V:Z1FZWJK0J/DI]"I`"9`!90!90+74Q'$Q'D]&amp;D]*"7"I`"9`!90!90J74Q'$Q'DQ&amp;C&amp;+7]D'4'1G/3)2A]@-JJM@+5IJ"9[@7P/4_IKA&gt;1^7#J(BD6A[$[A&amp;5@H/I$5&gt;VIV1V5X2D6'V;^%66!V=+K#65H[ILREDABRM12=5$M%\P%.N(PFP\FC6&gt;86\K]P.4*S9H'Y\'/DIZU=(#A`@V^\?\O;HN\7`V_`_:NN57@NW&lt;R8DLEGHB/P#3?%%_*0P'-?%']\?9MZHZNJ]U(D:LDH[0GUY^2-`\7)`;;D[^'*9[`\/(VG..L2FO^ZP-&lt;PP&gt;[:&gt;X\VWL?@3`VGPF\[8^Y._K2WK880(PU#Q&gt;Z^R!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">302022660</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="Serialized ACL" Type="Bin">%A#!"!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="dataflow" Type="Folder">
		<Item Name="dataflow demo 1.vi" Type="VI" URL="../dataflow demo 1.vi"/>
		<Item Name="dataflow demo 2.vi" Type="VI" URL="../dataflow demo 2.vi"/>
		<Item Name="dataflow demo 3.vi" Type="VI" URL="../dataflow demo 3.vi"/>
		<Item Name="dataflow demo 4.vi" Type="VI" URL="../dataflow demo 4.vi"/>
	</Item>
	<Item Name="FGV" Type="Folder">
		<Item Name="FGV demo action.ctl" Type="VI" URL="../FGV demo action.ctl"/>
		<Item Name="FGV demo FGV.vi" Type="VI" URL="../FGV demo FGV.vi"/>
		<Item Name="FGV demo main.vi" Type="VI" URL="../FGV demo main.vi"/>
		<Item Name="FGV demo read.vi" Type="VI" URL="../FGV demo read.vi"/>
		<Item Name="Global Stop.vi" Type="VI" URL="../Global Stop.vi"/>
	</Item>
	<Item Name="errorHandling" Type="Folder">
		<Item Name="error handling normal.vi" Type="VI" URL="../error handling normal.vi"/>
		<Item Name="error handling close.vi" Type="VI" URL="../error handling close.vi"/>
		<Item Name="error handling main.vi" Type="VI" URL="../error handling main.vi"/>
	</Item>
	<Item Name="basicTiming" Type="Folder">
		<Item Name="timingSimpleLoop.vi" Type="VI" URL="../timingSimpleLoop.vi"/>
		<Item Name="timingWaitLoop.vi" Type="VI" URL="../timingWaitLoop.vi"/>
		<Item Name="timingWaitMSLoop.vi" Type="VI" URL="../timingWaitMSLoop.vi"/>
		<Item Name="timingTimedLoop.vi" Type="VI" URL="../timingTimedLoop.vi"/>
	</Item>
	<Item Name="threadSynchronization" Type="Folder">
		<Item Name="occurrence_master-slave" Type="Folder">
			<Item Name="Continuously Generate Occurrences.vi" Type="VI" URL="../../../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/occurrence.llb/Continuously Generate Occurrences.vi"/>
		</Item>
		<Item Name="notifier_publisher-subscriber" Type="Folder">
			<Item Name="notifier subscriber.vi" Type="VI" URL="../notifier subscriber.vi"/>
			<Item Name="Notifier Demultiplexer.vi" Type="VI" URL="../../../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/notifier.llb/Notifier Demultiplexer.vi"/>
		</Item>
		<Item Name="queue_producer-consumer" Type="Folder">
			<Item Name="queue producer.vi" Type="VI" URL="../queue producer.vi"/>
			<Item Name="Queue Multiplexer.vi" Type="VI" URL="../../../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/queue.llb/Queue Multiplexer.vi"/>
		</Item>
	</Item>
	<Item Name="exclusiveAccess" Type="Folder">
		<Item Name="Semaphore with SubVIs.vi" Type="VI" URL="../../../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/semaphore.llb/Semaphore with SubVIs.vi"/>
	</Item>
	<Item Name="Rendezvous" Type="Folder">
		<Item Name="Rendezvous with SubVIs.vi" Type="VI" URL="../../../../../../../../Program Files (x86)/National Instruments/LabVIEW 2012/examples/general/rendezvous.llb/Rendezvous with SubVIs.vi"/>
	</Item>
	<Item Name="eventStructureGUI" Type="Folder">
		<Item Name="eventStructure GUI.vi" Type="VI" URL="../eventStructure GUI.vi"/>
	</Item>
	<Item Name="Reentrant" Type="Folder">
		<Item Name="Reentrant Sub.vi" Type="VI" URL="../Reentrant Sub.vi"/>
		<Item Name="Reentrant Main.vi" Type="VI" URL="../Reentrant Main.vi"/>
	</Item>
	<Item Name="StateMachine" Type="Folder">
		<Item Name="Simple State Machine - Traffic Light.vi" Type="VI" URL="../Simple State Machine - Traffic Light.vi"/>
		<Item Name="Traffic Light States.ctl" Type="VI" URL="../Traffic Light States.ctl"/>
	</Item>
</Library>
